from flask import Flask
from application.controllers import load_controllers
from application.config import load_configs

def create_app():
    app = Flask(__name__)
    register_blueprints(app)
    set_config_from_files(app)
    return app

#블루프린트-라우터 등록
def register_blueprints(app):
    for item in load_controllers():
        app.register_blueprint(item.controller)

#config 등록 
def set_config_from_files(app):
    for item in load_configs():
        app.config.from_object(item)
