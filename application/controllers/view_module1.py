from flask import (
    Blueprint, render_template
)

controller = Blueprint('view1', __name__, url_prefix='/view1')

@controller.route('')
def view1_main():
    return render_template('view1.html')