from flask import (
    Blueprint, render_template
)

controller = Blueprint('view2', __name__, url_prefix='/view2')

@controller.route('')
def view1_main():
    return render_template('view2.html')